# Focus Client Portal

Winning entry for the 2013 Focus Geomatics Code-a-thon. 

Written in a weekend using Python & Django.

![Login](https://gitlab.com/mmkiff/clientportalcodeathon/raw/master/Screenshots/1%20-%20Login.jpg)

![Client home](https://gitlab.com/mmkiff/clientportalcodeathon/raw/master/Screenshots/2%20-%20Client%20home.jpg)

![Messages](https://gitlab.com/mmkiff/clientportalcodeathon/raw/master/Screenshots/8%20-%20Messages.jpg)

![Wizard](https://gitlab.com/mmkiff/clientportalcodeathon/raw/master/Screenshots/clp14.jpg)

[More screenshots here.](https://gitlab.com/mmkiff/clientportalcodeathon/tree/master/Screenshots)