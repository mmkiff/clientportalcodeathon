from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^$', 'ClientPortal.views.index'),
    url(r'^logout/', 'ClientPortal.views.logout'),
    url(r'^login/', 'ClientPortal.views.login'),
    url(r'^viewJobs/', 'ClientPortal.views.viewJobs'),
    url(r'^viewJobFiles/', 'ClientPortal.views.viewJobFiles'),
    url(r'^viewJobHistory/', 'ClientPortal.views.viewJobHistory'),
    url(r'^home/', 'ClientPortal.views.home'),
    url(r'^getMessage/', 'ClientPortal.views.getMessage'),
    url(r'^getJobDocuments/', 'ClientPortal.views.getJobDocuments'),
    url(r'^getJobHistory/', 'ClientPortal.views.getJobHistory'),
    url(r'^resetAndEmailPassword', 'ClientPortal.views.resetAndEmailPassword'),
    url(r'^forgotPassword', 'ClientPortal.views.forgotPassword'),
    url(r'^getContactsForClient', 'ClientPortal.views.getContactsForClient'),
    url(r'^changePassword', 'ClientPortal.views.changePassword'),
    url(r'^performChangePassword', 'ClientPortal.views.performChangePassword'),
    url(r'^userViewedJobs', 'ClientPortal.views.userViewedJobs'),
    url(r'^userViewedMessage', 'ClientPortal.views.userViewedMessage'),
    url(r'^viewMessages', 'ClientPortal.views.viewMessages'),
    url(r'^clientFiles', 'ClientPortal.views.clientFiles'),
    url(r'^uploadFile', 'ClientPortal.views.uploadFile'),
    url(r'^sendMessage', 'ClientPortal.views.sendMessage'),
    url(r'^clientActivity', 'ClientPortal.views.clientActivity'),
    url(r'^editJob', 'ClientPortal.views.editJob'),
    url(r'^manageJobs', 'ClientPortal.views.manageJobs'),
    url(r'^comments/', include('django.contrib.comments.urls')),
    # url(r'^logout/', 'ClientPortal.views.logout'),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
