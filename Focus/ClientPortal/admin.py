from ClientPortal.models import *
from django.contrib import admin

class JobAdmin(admin.ModelAdmin):
    list_display = ('number', 'status', 'client')
    search_fields = ('number', 'status')
    radio_fields = {"status":admin.HORIZONTAL}
    list_filter = ('status',)

admin.site.register(Job, JobAdmin)
admin.site.register(Location)
admin.site.register(UserAccount)
admin.site.register(UserActivity)
admin.site.register(Client)
admin.site.register(JobFile)
admin.site.register(JobHistory)
admin.site.register(Message)

