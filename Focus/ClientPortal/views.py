from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.contrib import auth
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
import json
import string
import random
from models import *
import datetime
from django.core import serializers
from django import forms
from django.db.models import Count


def index(request):
    message = request.session.get('LoginMessage', '')
    if request.user.is_authenticated():
        return home(request)
    else:
        return render_to_response('index.html', {'message': message}, context_instance=RequestContext(request))


@login_required
def home(request):
    user = request.user.get_profile()
    if user.role == 3:
        return viewJobs(request)
    else:
        return pmHome(request)


@login_required
def pmHome(request):
    user = request.user.get_profile()
    if user.role == 3:
        return viewJobs(request)
    else:
        return manageJobs(request)


@login_required
def clientActivity(request):
    user = request.user.get_profile()
    unreadMessages = len(Message.objects.filter(sentTo__userId=user.userId).filter(hasBeenRead=False))
    clientActivity = UserActivity.objects.filter(user__role=3).order_by('-date')
    clientList = Client.objects.all().exclude(clientId='0001')
    return render_to_response('clientActivity.html', {'user': user, 'clientActivity': clientActivity, 'unreadMessages': unreadMessages, 'clientList': clientList })


@login_required
def viewJobs(request):
    user = request.user.get_profile()
    unreadMessages = len(Message.objects.filter(sentTo__userId=user.userId).filter(hasBeenRead=False))
    jobs = Job.objects.filter(client__clientId=user.client.clientId).annotate(numberOfFiles=Count('jobfile'))
    jobStatusList = Job.JOB_STATUS_CHOICES
    jobTypeList = Job.JOB_TYPE_CHOICES
    jobsJson = serializers.serialize("json", jobs)
    history = JobHistory.objects.filter(job__client=user.client.clientId).order_by('date')
    numberOfChangesSinceLastLogin = len(Job.objects.filter(client__clientId=user.client.clientId).filter(lastModifiedDate__gte=user.lastViewedJobs))
    return render_to_response('viewJobs.html', {'user': request.user.get_profile(), 'unreadMessages': unreadMessages, 'numberOfChangesSinceLastLogin': numberOfChangesSinceLastLogin, 'jobStatusList': jobStatusList, 'jobTypeList': jobTypeList, 'history': history, 'jobs': jobs, 'jobsJson': jobsJson })


@login_required
def manageJobs(request):
    user = request.user.get_profile()
    unreadMessages = len(Message.objects.filter(sentTo__userId=user.userId).filter(hasBeenRead=False))
    jobs = Job.objects.all().annotate(numberOfFiles=Count('jobfile'))
    jobStatusList = Job.JOB_STATUS_CHOICES
    jobTypeList = Job.JOB_TYPE_CHOICES
    jobsJson = serializers.serialize("json", jobs)
    history = JobHistory.objects.all().order_by('date')
    fileForm = UploadFileForm()
    numberOfChangesSinceLastLogin = len(Job.objects.filter(lastModifiedDate__gte=user.lastViewedJobs))
    return render_to_response('manageJobs.html', {'user': request.user.get_profile(), 'fileForm': fileForm, 'unreadMessages': unreadMessages, 'numberOfChangesSinceLastLogin': numberOfChangesSinceLastLogin, 'jobStatusList': jobStatusList, 'jobTypeList': jobTypeList, 'history': history, 'jobs': jobs, 'jobsJson': jobsJson })



def userViewedJobs(request):
    currentUser = request.GET.get('user', '')
    user = UserAccount.objects.get(userId=currentUser)
    user.lastViewedJobs = datetime.datetime.now()
    user.save()
    activity = UserActivity()
    activity.date = datetime.datetime.now()
    activity.user = user
    activity.action = "Viewed their jobs."
    activity.save()
    result = JsonResult(True, currentUser)
    return HttpResponse(result.toJson(), content_type="application/json")


def userViewedMessage(request):
    currentUser = request.GET.get('user', '')
    user = UserAccount.objects.get(userId=currentUser)
    messageId = request.GET.get('messageId', '')
    message = Message.objects.get(id=messageId)
    message.hasBeenRead = True
    message.save()
    activity = UserActivity()
    activity.date = datetime.datetime.now()
    activity.user = user
    activity.action = "Read the message titled '" + message.subject + "' send by " + message.sentFrom.user.first_name + " " + message.sentFrom.user.last_name + " on " + str(message.date) + "."
    activity.save()
    result = JsonResult(True, '')
    return HttpResponse(result.toJson(), content_type="application/json")


def logout(request):
    auth.logout(request)
    return index(request)


def login(request):
    username = request.POST['user']
    password = request.POST['pw']
    user = auth.authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            auth.login(request, user)
            return home(request)
        else:
            request.session['LoginMessage'] = "Your account has been disabled. Please contact <a href=''>support@focus.ca</a> for assistance."
            return index(request) #, "Account disabled.")
    else:
        request.session['LoginMessage'] = "Incorrect username or password."
        return index(request) #, "Invalid username/pw")


def forgotPassword(request):
    return render_to_response('forgotPassword.html', {}, context_instance=RequestContext(request))


def changePassword(request):
    user = request.user.get_profile()
    return render_to_response('changePassword.html', {'user': user}, context_instance=RequestContext(request))


def resetAndEmailPassword(request):
    emailAddress = request.GET.get('email', '')
    result = JsonResult(True, '')
    userToReset = User.objects.filter(email=emailAddress)
    if len(userToReset) > 0:
        randomPassword = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(8))
        userToReset[0].set_password(randomPassword)
        userToReset[0].save()
        send_mail('Focus Client Portal - Password Reset', 'Your password has been reset. New password: ' + randomPassword + '.<br/><br/>(would have sent to ' + emailAddress + ')', 'clientportal@newfrost.com',
                  ['mmkiff@gmail.com'], fail_silently=False)
    return HttpResponse(result.toJson(), content_type="application/json")


@login_required
def performChangePassword(request):
    user = request.user
    currentPassword = request.POST.get('currentPassword', '')
    password1 = request.POST.get('newPassword1', '')
    password2 = request.POST.get('newPassword2', '')
    if (password1 == password2) and (password1 != ''):  # TO DO - more validation
        user.set_password(password1)
        user.save()
        result = JsonResult(True, '')
    else:
        result = JsonResult(False, 'Passwords did not match.')
    return HttpResponse(result.toJson(), content_type="application/json")


def getJobHistory(request):
    jobNumber = request.GET.get('jobNumber', 'NOJOB')
    history = JobHistory.objects.filter(job__number=jobNumber)
    data = serializers.serialize("json", history)
    return HttpResponse(data, content_type="application/json")


def getMessage(request):
    messageId = request.GET.get('messageId', 'NOID')
    message = Message.objects.filter(id=messageId)
    data = serializers.serialize("json", message)
    return HttpResponse(data, content_type="application/json")


def getJobDocuments(request):
    jobNumber = request.GET.get('jobNumber', 'NOJOB')
    files = JobFile.objects.filter(job__number=jobNumber)
    data = serializers.serialize("json", files)
    return HttpResponse(data, content_type="application/json")


@login_required
def viewMessages(request):
    user = request.user.get_profile()
    unreadMessages = len(Message.objects.filter(sentTo__userId=user.userId).filter(hasBeenRead=False))
    clientList = Client.objects.all().exclude(clientId='0001')
    usersMessages = Message.objects.filter(sentTo__userId=user.userId)
    usersSentMessages = Message.objects.filter(sentFrom__userId=user.userId)
    return render_to_response('messages.html', { 'user': user, 'unreadMessages': unreadMessages, 'clientList': clientList, 'messages': usersMessages, 'sentMessages': usersSentMessages }, context_instance=RequestContext(request))


@login_required
def sendMessage(request):
    user = request.user.get_profile()
    subject = request.POST.get('subject')
    to = request.POST.get('to')
    message = request.POST.get('message')
    m = Message()
    m.date = datetime.datetime.now()
    m.sentTo = UserAccount.objects.get(userId=to)
    m.sentFrom = user
    m.subject = subject
    m.message = message
    m.save()
    # send an email
    result = JsonResult(True, '')
    return HttpResponse(result.toJson(), content_type="application/json")


@login_required
def getContactsForClient(request):
    clientId = request.GET.get('clientId', 'NOCLIENT')
    contacts = User.objects.filter(useraccount__client__clientId=clientId)
    data = serializers.serialize("json", contacts)
    return HttpResponse(data, content_type="application/json")


@login_required
def sendMessage(request):
    user = request.user.get_profile()
    subject = request.POST.get('subject')
    to = request.POST.get('to')
    message = request.POST.get('message')
    m = Message()
    m.date = datetime.datetime.now()
    m.sentTo = UserAccount.objects.get(user__username=to)
    m.sentFrom = user
    m.subject = subject
    m.message = message
    m.save()
    # send an email
    result = JsonResult(True, '')
    return HttpResponse(result.toJson(), content_type="application/json")


@login_required
def sendMessage(request):
    user = request.user.get_profile()
    subject = request.POST.get('subject')
    to = request.POST.get('to')
    message = request.POST.get('message')
    m = Message()
    m.date = datetime.datetime.now()
    m.sentTo = UserAccount.objects.get(userId=to)
    m.sentFrom = user
    m.subject = subject
    m.message = message
    m.save()
    result = JsonResult(True, '')
    return HttpResponse(result.toJson(), content_type="application/json")


@login_required
def viewJobFiles(request):
    user = request.user.get_profile()
    unreadMessages = len(Message.objects.filter(sentTo__userId=user.userId).filter(hasBeenRead=False))
    files = JobFile.objects.filter(job__client__clientId=user.client.clientId).order_by('-dateAdded')
    return render_to_response('viewJobFiles.html', {'user': request.user.get_profile(), 'unreadMessages': unreadMessages, 'files': files })


@login_required
def viewJobHistory(request):
    user = request.user.get_profile()
    if user.role == 3:
        history = JobHistory.objects.filter(job__client__clientId=user.client.clientId).order_by('-date')
    else:
        history = JobHistory.objects.all().order_by('-date')
    unreadMessages = len(Message.objects.filter(sentTo__userId=user.userId).filter(hasBeenRead=False))
    return render_to_response('viewJobHistory.html', {'user': request.user.get_profile(), 'unreadMessages': unreadMessages, 'history': history})


@login_required
def sendMessage(request):
    user = request.user.get_profile()
    subject = request.POST.get('subject')
    to = request.POST.get('to')
    message = request.POST.get('message')
    m = Message()
    m.date = datetime.datetime.now()
    m.sentTo = UserAccount.objects.get(user__email=to)
    m.sentFrom = user
    m.subject = subject
    m.message = message
    m.save()
    # send an email
    result = JsonResult(True, '')
    return HttpResponse(result.toJson(), content_type="application/json")


@login_required()
def editJob(request):
    jobNumber = request.POST.get('jobNumber')
    eta = request.POST.get('eta')
    status = int(request.POST.get('status'))
    comment = request.POST.get('comment')
    job = Job.objects.filter(number=jobNumber)[0]
    job.status = status
    job.save()
    result = JsonResult(True, '')
    return HttpResponse(result.toJson(), content_type="application/json")


@login_required
def clientFiles(request):
    user = request.user.get_profile()
    unreadMessages = len(Message.objects.filter(sentTo__userId=user.userId).filter(hasBeenRead=False))
    fileForm = UploadFileForm()
    return render_to_response('clientFiles.html', {'user': request.user.get_profile(), 'fileForm': fileForm, 'unreadMessages': unreadMessages })


def uploadFile(request):
    form = UploadFileForm(request.POST, request.FILES)
    file = request.FILES['file']
    fileType = request.POST.get('fileType')
    jobNumber = request.POST.get('jobNumber')
    jobFile = JobFile()
    jobFile.dateAdded = datetime.datetime.now()
    jobFile.file = file
    jobFile.fileType = fileType
    jobFile.job = Job.objects.get(number=jobNumber)
    jobFile.save()
    return HttpResponseRedirect('/home')


class UploadFileForm(forms.Form):
    FILE_TYPES = (
        (1, 'Invoice'),
        (2, 'Plan'),
        (3, 'Misc'),
    )
    file = forms.FileField()
    fileType = forms.ChoiceField(choices=FILE_TYPES)
    jobNumber = forms.CharField(max_length=50)


class JsonResult:
    success = True
    message = ""

    def __init__(self, success, message):
        self.success = success
        self.message = message

    def toJson(self):
        result = {}
        result['success'] = self.success
        result['message'] = self.message
        return json.dumps(result)

