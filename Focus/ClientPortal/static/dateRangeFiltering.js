/**
 * Created with PyCharm.
 * User: m
 * Date: 21/02/13
 * Time: 8:12 PM
 * To change this template use File | Settings | File Templates.
 */

$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
        var iMin = document.getElementById('min').value * 1;
        var iMax = document.getElementById('max').value * 1;
        var iVersion = aData[3] == "-" ? 0 : aData[3]*1;
        if ( iMin == "" && iMax == "" )
        {
            return true;
        }
        else if ( iMin == "" && iVersion < iMax )
        {
            return true;
        }
        else if ( iMin < iVersion && "" == iMax )
        {
            return true;
        }
        else if ( iMin < iVersion && iVersion < iMax )
        {
            return true;
        }
        return false;
    }
);