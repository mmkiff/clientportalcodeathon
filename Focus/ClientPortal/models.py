from django.db import models
from django.db.models.signals import pre_save
from django.db.models.signals import post_save
from django.contrib.auth.models import User
import datetime


class Job(models.Model):
    JOB_STATUS_CHOICES = (
        (1, 'New Job'),
        (2, 'Job Setup Completed'),
        (3, 'Released To Field'),
        (4, 'Field Work Started'),
        (5, 'Field Work Completed'),
        (6, 'Drafting Started'),
        (7, 'Drafting Completed'),
        (8, 'Checking Started'),
        (9, 'Checking Completed'),
        (10, 'Completed'),
        (11, 'Cancelled'),
    )
    JOB_TYPE_CHOICES = (
        (1, 'Pipeline'),
        (2, 'Wellsite'),
        (3, 'Core Hole'),
        (4, 'Right of Way'),
        (5, 'Tower Site'),
        (6, 'Campsite'),
        (7, 'Other'),
    )
    INVOICE_STATUS_CHOICES = (
        (1, 'None'),
        (2, 'Ready'),
        (3, 'Paid'),
    )
    number = models.CharField(max_length=50)
    status = models.IntegerField(choices=JOB_STATUS_CHOICES)
    jobType = models.IntegerField(choices=JOB_TYPE_CHOICES)
    location_surface_from = models.ForeignKey('Location', related_name='surfaceFrom')
    location_bottom_to = models.ForeignKey('Location', related_name='bottomTo')
    client = models.ForeignKey('Client')
    afeNumber = models.CharField(max_length=50, blank=True)
    clientFileNumber = models.CharField(max_length=50, blank=True)
    notes = models.CharField(max_length=500, blank=True)
    startDate = models.DateField()
    expectedCompletionDate = models.DateField()
    lastModifiedDate = models.DateTimeField()
    invoiceStatus = models.IntegerField(choices=INVOICE_STATUS_CHOICES)
    assignedTo = models.CharField(max_length=100)

    def __unicode__(self):
        return self.number

    def save(self, force_insert=False, force_update=False):
        self.lastModifiedDate = datetime.datetime.now()
        super(Job, self).save(force_insert, force_update)


class Location(models.Model):
    # PNG
    QuarterUnit = models.CharField(max_length=1, blank=True, choices=((i, i) for i in ('A', 'B', 'C', 'D')))  # A - D
    Unit = models.IntegerField(null=True, blank=True, choices=((i, i) for i in range(1, 101)))  # 1 - 100
    Block = models.CharField(max_length=1, blank=True, choices=((i, i) for i in ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L')))  # A - L
    NTSMapsheet = models.IntegerField(null=True, blank=True, choices=((i, i) for i in (82, 83, 92, 93, 94, 102, 103, 104, 114)))
    Mapsheet1_250 = models.CharField(max_length=1, blank=True, choices=((i, i) for i in ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P')))  # A - P
    Mapsheet1_50 = models.IntegerField(null=True, blank=True, choices=((i, i) for i in range(1, 17)))  # 1 - 16
    # ATS
    LSD = models.IntegerField(null=True, blank=True, choices=((i, i) for i in range(1, 17)))  # 1 - 16
    Section = models.IntegerField(null=True, blank=True, choices=((i, i) for i in range(1, 37)))  # 1 - 36
    Township = models.IntegerField(null=True, blank=True, choices=((i, i) for i in range(1, 127)))  # 1 - 126
    Range = models.IntegerField(null=True, blank=True, choices=((i, i) for i in range(1, 31)))  # 1 - 30
    Meridian = models.IntegerField(null=True, blank=True, choices=((i, i) for i in range(1, 7)))  # 1 - 6
    # lat / long
    Latitude = models.FloatField()
    Longitude = models.FloatField()

    def __unicode__(self):
        if self.LSD is not None or self.Section is not None or self.Township is not None or self.Range is not None or self.Meridian is not None:
            return xstr(self.LSD) + '-' + xstr(self.Section) + '-' + xstr(self.Township) + '-' + xstr(self.Range) + '-' + xstr(self.Meridian)
            #return 'LSD LOCATION'
        else:
            return self.QuarterUnit + '-' + xstr(self.Unit) + '-' + self.Block + "-" + xstr(self.NTSMapsheet) + "-" + self.Mapsheet1_250 + "-" + xstr(self.Mapsheet1_50)
            #return 'PNG LOCATION'



class UserAccount(models.Model):
    ROLES = (
        (1, 'FocusEmployee'),
        (2, 'FocusPM'),
        (3, 'ClientContact'),
    )
    user = models.OneToOneField(User)
    userId = models.IntegerField(unique=True)
    client = models.ForeignKey('Client', blank=True)
    active = models.BooleanField(default=True)
    role = models.IntegerField(choices=ROLES, blank=True)
    lastViewedJobs = models.DateTimeField()

    def __unicode__(self):
        return self.user.username + " (" + str(self.userId) + ")"


class Client(models.Model):
    clientId = models.IntegerField(max_length=4)
    name = models.CharField(max_length=500)
    logo = models.FileField(upload_to='ClientPortal/static/ClientLogos', blank=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name + " (" + str(self.clientId) + ")"


class JobHistory(models.Model):
    job = models.ForeignKey('Job')
    date = models.DateTimeField()
    change = models.CharField(max_length=500)

    def __unicode__(self):
        return self.job.number + " - " + str(self.date) + " - " + self.change


class JobFile(models.Model):
    FILE_TYPES = (
        (1, 'Invoice'),
        (2, 'Plan'),
        (3, 'Misc'),
    )
    dateAdded = models.DateTimeField(auto_now_add=True)
    file = models.FileField(upload_to='ClientPortal/static/Files')
    fileType = models.IntegerField(choices=FILE_TYPES, blank=True)
    job = models.ForeignKey('Job', blank=True)
    isInvoice = models.BooleanField(default=False)

    def __unicode__(self):
        return self.job.number + '-' + self.file.name


class UserActivity(models.Model):
    date = models.DateTimeField()
    user = models.ForeignKey('UserAccount', blank=True)
    action = models.CharField(max_length=500)

    def __unicode__(self):
        return str(self.date) + "-" + self.user.user.email + " - " + self.action


class Message(models.Model):
    date = models.DateTimeField()
    sentFrom = models.ForeignKey('UserAccount', related_name='sentFrom')
    sentTo = models.ForeignKey('UserAccount', related_name='sentTo')
    subject = models.CharField(max_length=400)
    message = models.TextField()
    hasBeenRead = models.BooleanField(default=False)
    attachment = models.FileField(upload_to='ClientPortal/static/MessageAttachments', blank=True)

    def __unicode__(self):
        return str(self.date) + " - from " + self.sentFrom.user.email + " to " + self.sentTo.user.email + " - " + self.subject


def xstr(s):
    return '' if s is None else str(s)


def jobChangedHandler(sender, instance, **kwargs):
    originalJob = Job.objects.filter(number=instance.number)
    if len(originalJob) > 0:
        originalJob = originalJob[0]
        if originalJob.afeNumber != instance.afeNumber:
            history = JobHistory()
            history.date = datetime.datetime.now()
            history.job = originalJob
            history.change = "AFE Number changed from '" + originalJob.afeNumber + "' to '" + instance.afeNumber + "'."
            history.save()
        if originalJob.clientFileNumber != instance.clientFileNumber:
            history = JobHistory()
            history.date = datetime.datetime.now()
            history.job = originalJob
            history.change = "Client File Number changed from '" + originalJob.clientFileNumber + "' to '" + instance.clientFileNumber + "'."
            history.save()
        if originalJob.jobType != instance.jobType:
            history = JobHistory()
            history.date = datetime.datetime.now()
            history.job = originalJob
            history.change = "Job Type changed from '" + originalJob.get_jobType_display() + "' to '" + instance.get_jobType_display() + "'."
            history.save()
        if originalJob.status != instance.status:
            history = JobHistory()
            history.date = datetime.datetime.now()
            history.job = originalJob
            history.change = "Status changed from '" + originalJob.get_status_display() + "' to '" + instance.get_status_display() + "'."
            history.save()


def jobFileChangedHandler(sender, instance, **kwargs):
    history = JobHistory()
    history.date = datetime.datetime.now()
    history.job = instance.job
    history.change = "Added file '" + instance.file.name + "'."
    history.save()


pre_save.connect(jobChangedHandler, sender=Job)
post_save.connect(jobFileChangedHandler, sender=JobFile)
